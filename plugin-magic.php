<?php
/**
 * Plugin Name: Plugin Magic
 * Plugin URI: https://wpdesk.pl/
 * Description: Run by shortcode: [plugin_magic].
 * Version: 1.0.0
 * Author: WP Desk
 * Author URI: https://www.wpdesk.pl/
 * Text Domain: magic-plugin
 * Domain Path: /lang/
 * Requires at least: 4.5
 * Tested up to: 5.2.2
 * WC requires at least: 3.1.0
 * WC tested up to: 3.7.0
 * Requires PHP: 5.6
 *
 * Copyright 2017 WP Desk Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package PluginMagic
 */

class WPDesk_Plugin_Magic {

	/**
	 * Target URL.
	 *
	 * @var string|null
	 */
	private $target_url;

	/**
	 * Plugin list.
	 *
	 * @var array
	 */
	private $plugin_list;

	/**
	 * Magic_Plugin constructor.
	 */
	public function __construct() {
		require_once 'plugin-magic-lists.php';
		$this->plugin_list = ( new WP_Desk_Plugin_Magic_list() )->get_list();
		$this->hooks();

	}

	/**
	 * Fires hooks.
	 */
	public function hooks() {
		add_shortcode( 'plugin_magic', [ $this, 'plugin_magic_shortcode' ] );
		add_action( 'wp_ajax_plugin_magic_check', [ $this, 'ajax_process_validate' ] );
		add_action( 'wp_ajax_nopriv_plugin_magic_check', [ $this, 'ajax_process_validate' ] );
	}

	/**
	 * Plugin magic shortcode.
	 *
	 * @return string
	 */
	public function plugin_magic_shortcode() {
		ob_start();
		?>
		<form method="GET" name="plugin-magic">
			<input size="42" type="url" name="plugin_magic_siteurl" class="long-text"/>
			<button type="button" class="plugin_magic_run">Uruchom</button>
			<div class="plugin-team">
				<label><input type="radio" value="heroes" name="plugin_magic_team">wtyczki heroes</input></label>
				<label><input type="radio" value="predators" name="plugin_magic_team">wtyczki predators</input></label>
				<label><input type="radio" value="both" name="plugin_magic_team">wszystkie</input></label>
			</div>
			<div><a href="#" class="select-plugin">Filtrowanie</a></div>
			<div class="plugin-list">
				<p>Zawęź sprawdzanie do wybranych wtyczek:</p>
				<?php
				foreach ( $this->plugin_list as $plugin_slug => $plugin ) {
					$checked = '';
					if( $plugin_slug === 'woocommerce' ) {
						$checked = 'checked';
					}
					echo '<div><label><input ' . $checked . ' type="checkbox" name="filter_plugins" class="type-'. $plugin['team'] . '" value="' . $plugin_slug . '" /> ' . $plugin['name'] . '</label></div>';
				}
				?>
			</div>
		</form>

		<table class="wide-flat plugin-magic" style="display: none;">
			<thead>
			<tr>
				<th width="80%">Nazwa</th>
				<th width="10%">Klient</th>
				<th width="10%">Repo</th>
			</tr>
			</thead>
			<tbody id="plugin-magic-list">
			</tbody>
		</table>

		<script type="text/javascript">
			// <![CDATA[

			function validURL(str) {
				var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
					'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
					'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
					'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
					'(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
					'(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
				return !!pattern.test(str);
			}

			jQuery(document).ready(function ($) {
				$('.select-plugin').click(function () {
					$('.plugin-list').toggle();
					return false;
				});

				var ajax_url = '<?php echo admin_url( 'admin-ajax.php' ); ?>';

				var _form = $('form[name="plugin-magic"]');
				var _plugin_list = $('#plugin-magic-list');
				var plugins = <?php echo json_encode( $this->plugin_list ); ?>;


				$('input[name="plugin_magic_team"]').change(function() {
					var team = $(this).val();
					$('.plugin-list input').not(this).prop('checked', false);
					if( team === 'heroes' || team === 'predators' ) {
						$('.plugin-list .type-both').not(this).prop('checked', true );
						$('.plugin-list .type-' + team).not(this).prop('checked', true );
					} else {
						$('.plugin-list input').not(this).prop('checked', true);
					}
				});

				$('.plugin_magic_run').click(function () {
					var input_siteurl = $('input[name="plugin_magic_siteurl"]');
					var siteurl = input_siteurl.val();
					if (!siteurl || !validURL(siteurl)) {
						input_siteurl.css('border', '1px solid red');
						return false;
					}

					_form.hide();
					_plugin_list.html('');
					$('table.plugin-magic').show();
					var input_plugins = $('input[name="filter_plugins"]').serializeArray();

					// Filter selected plugins.
					var filtered_plugins = {};
					$.each(input_plugins, function (index, row) {
						filtered_plugins[row.value] = plugins[row.value];
					});
					if (Object.keys(filtered_plugins).length > 0) {
						plugins = filtered_plugins;
					}

					$.each(plugins, function (index, value) {
						$.ajax({
							type: "POST",
							async: false,
							url: ajax_url + '?action=plugin_magic_check',
							data: 'slug=' + index + '&free=' + value.free + '&target_url=' + siteurl,
							success: function (response) {
								var tr_class = '';
								if ('unknown' !== response.data.version) {
									tr_class = 'exists';
								} else {
									response.data.version = 'brak';
									tr_class = 'notexists';
								}

								if( tr_class === 'exists' && response.data.version !== response.data.current ) {
									tr_class = 'notequal'
								}

								_plugin_list.append('<tr class="' + tr_class + '"><td class="plugin-name">' + value.name + '</td><td>' + response.data.version + '</td><td>' + response.data.current + '</td></tr>');
							}
						});
					});
					return false;
				});

			});
			// ]]>
		</script>
		<style>
			.plugin-list {
				display: none;
			}

			table {
				border-bottom: 1px solid #DDD;
				border-collapse: collapse;
			}

			table.plugin-magic td {
				border-bottom: 1px solid #DDD;
				padding: 8px 8px;
			}

			tr.exists td {
				background-color: seagreen !important;;
				color: #FFF !important;;
			}

			tr.notexists td {
				background-color: #d65a4f !important;
				color: #FFF !important;;
			}

			tr.notequal td {
				background-color: #ebbc3d !important;
				color: #FFF !important;;
			}

		</style>
		<?php
		$output = ob_get_contents();
		ob_end_clean();

		return $output;
	}

	/**
	 * Get URL host.
	 *
	 * @param string $url URL.
	 *
	 * @return string
	 */
	private function get_host_domain( $url ) {
		$result = parse_url( $url );

		return $result['scheme'] . "://" . $result['host'];
	}

	/**
	 * Validate version through AJAX.
	 */
	public function ajax_process_validate() {
		$this->target_url = esc_url( $this->get_host_domain( $_POST['target_url'] ) );
		$free             = sanitize_key( esc_attr( $_POST['free'] ) );
		$slug             = sanitize_key( esc_attr( $_POST['slug'] ) );
		$current          = $this->get_repository_plugin_version( $slug, $free );
		$version          = $this->get_client_plugin_version( $slug, $free );
		wp_send_json_success( [ 'current' => $current, 'version' => $version ] );
	}

	/**
	 * Get repository plugin current version.
	 *
	 * @param string $id   Plugin slug.
	 * @param string $free Is free?
	 *
	 * @return string
	 */
	public function get_repository_plugin_version( $id, $free ) {
		if ( 'false' === $free ) {
			$url      = 'https://gitlab.com/wpdesk/' . $id . '/-/tags?feed_token=wjGv_kVnR77xZicY5x8E&format=atom';
			$response = wp_remote_get( $url );
			if ( 200 === $response['response']['code'] && ! empty( isset( $response['body'] ) ) ) {
				$xml = simplexml_load_string( $response['body'] );

				return isset( $xml->entry[0]->title ) ? (string) $xml->entry[0]->title : 'unknown';
			}
		} else {
			$url      = "https://api.wordpress.org/plugins/info/1.2/?action=plugin_information&request[slugs][]={$id}";
			$response = wp_remote_get( $url );
			if ( 200 === $response['response']['code'] && ! empty( isset( $response['body'] ) ) ) {
				$plugins = json_decode( $response['body'] );
				$version = 'none';
				foreach ( $plugins as $key => $plugin ) {

					if ( ! isset( $plugin->error ) && isset( $plugin->version ) ) {
						$version = $plugin->version;
					}

				}

				return $version;
			}

		}

		return 'unknown';
	}

	/**
	 * Get client plugin version.
	 *
	 * @param string $id   Plugin slug.
	 * @param string $free Is free?
	 *
	 * @return string
	 */
	public function get_client_plugin_version( $id, $free ) {
		$directory_url = $this->target_url . '/wp-content/plugins/' . $id;
		$file          = 'changelog.txt';
		if ( 'true' === $free ) {
			$file = 'readme.txt';
		}
		$url      = $directory_url . '/' . $file;
		$response = wp_remote_get( $url );
		if ( 200 === $response['response']['code'] && ! empty( isset( $response['body'] ) ) ) {
			$content = $response['body'];
			if ( 'true' === $free ) {
				preg_match( '/Stable tag:(.+)/i', $content, $matches );
				if ( isset( $matches[1] ) ) {
					return trim( $matches[1] );
				}
			} else {
				preg_match( '/([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}).+/i', $content, $matches );
				if ( isset( $matches[1] ) ) {
					return trim( $matches[1] );
				}
			}
		}

		return 'unknown';
	}

}

new WPDesk_Plugin_Magic();