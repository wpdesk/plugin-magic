<?php

class WP_Desk_Plugin_Magic_list {

	public function get_list() {
		return [
			'woocommerce'                             => [
				'free' => true,
				'name' => 'WooCommerce',
				'team' => 'both',
			],
			'ceneo-trusted-reviews'                   => [
				'free' => false,
				'name' => 'Ceneo Zaufane Opinie',
				'team' => 'heroes',
			],
			'flexible-checkout-fields'                => [
				'free' => false,
				'name' => 'WooCommerce Flexible Checkout Fields',
				'team' => 'predators',
			],
			'flexible-checkout-fields-pro'            => [
				'free' => false,
				'name' => 'WooCommerce Flexible Checkout Fields Pro',
				'team' => 'predators',
			],
			'flexible-invoices'                       => [
				'free' => true,
				'name' => 'Flexbile Invoices',
				'team' => 'heroes',
			],
			'flexible-invoices-reports'               => [
				'free' => false,
				'name' => 'Flexbile Invoices Reports',
				'team' => 'heroes',
			],
			'flexible-invoices-woocommerce'           => [
				'free' => false,
				'name' => 'Flexbile Invoices WooCommerce',
				'team' => 'heroes',
			],
			'flexible-printing'                       => [
				'free' => false,
				'name' => 'Flexbile Printing',
				'team' => 'predators',
			],
			'flexible-product-fields'                 => [
				'free' => true,
				'name' => 'Flexbile Product Fields',
				'team' => 'predators',
			],
			'flexible-product-fields-pro'             => [
				'free' => false,
				'name' => 'Flexbile Product Fields Pro',
				'team' => 'predators',
			],
			'flexible-shipping'                       => [
				'free' => true,
				'name' => 'Flexbile Shipping',
				'team' => 'predators',
			],
			'flexible-shipping-pro'                   => [
				'free' => false,
				'name' => 'Flexbile Shipping Pro',
				'team' => 'predators',
			],
			'flexible-shipping-fedex'                 => [
				'free' => true,
				'name' => 'Flexible Shipping For FedEx',
				'team' => 'predators',
			],
			'flexible-shipping-ups'                   => [
				'free' => true,
				'name' => 'Flexbile Shipping UPS',
				'team' => 'predators',
			],
			'flexible-shipping-ups-pro'               => [
				'free' => false,
				'name' => 'Flexbile Shipping UPS Pro',
				'team' => 'predators',
			],
			'woo-cart-weight'                         => [
				'free' => true,
				'name' => 'WooCommerce Cart Weight',
				'team' => 'heroes',
			],
			'woocommerce-active-payments'             => [
				'free' => false,
				'name' => 'WooCommerce Active Payments',
				'team' => 'heroes',
			],
			'woocommerce-allegro'                     => [
				'free' => false,
				'name' => 'WooCommerce Allegro',
				'team' => 'heroes',
			],
			'woocommerce-ceneo'                       => [
				'free' => false,
				'name' => 'WooCommerce Ceneo',
				'team' => 'heroes',
			],
			'woocommerce-dhl'                         => [
				'free' => false,
				'name' => 'WooCommerce DHL',
				'team' => 'predators',
			],
			'woocommerce-dpd'                         => [
				'free' => false,
				'name' => 'WooCommerce DPD',
				'team' => 'predators',
			],
			'woocommerce-dpd-uk'                      => [
				'free' => false,
				'name' => 'WooCommerce DPD UK',
				'team' => 'predators',
			],
			'woocommerce-enadawca'                    => [
				'free' => false,
				'name' => 'WooCommerce Enadawca',
				'team' => 'predators',
			],
			'woocommerce-fakturownia'                 => [
				'free' => false,
				'name' => 'WooCommerce Fakturownia',
				'team' => 'heroes',
			],
			'woocommerce-flexible-pricing'            => [
				'free' => false,
				'name' => 'WooCommerce Flexible Pricing',
				'team' => 'heroes',
			],
			'woocommerce-freshmail'                   => [
				'free' => false,
				'name' => 'WooCommerce FreshMail',
				'team' => 'heroes',
			],
			'woocommerce-furgonetka'                  => [
				'free' => false,
				'name' => 'WooCommerce Furgonetka',
				'team' => 'predators',
			],
			'woocommerce-gateway-dotpay'              => [
				'free' => false,
				'name' => 'WooCommerce Dotpay',
				'team' => 'heroes',
			],
			'woocommerce-gateway-payu-pl'             => [
				'free' => false,
				'name' => 'WooCommerce PayU',
				'team' => 'heroes',
			],
			'woocommerce-gateway-przelewy24'          => [
				'free' => false,
				'name' => 'WooCommerce Przelewy24',
				'team' => 'heroes',
			],
			'woocommerce-gateway-transferuj-pl'       => [
				'free' => false,
				'name' => 'WooCommerce Tpay.com (Transferuj.pl)',
				'team' => 'heroes',
			],
			'woocommerce-google-merchant-xml'         => [
				'free' => false,
				'name' => 'WooCommerce Google Merchant XML',
				'team' => 'heroes',
			],
			'woocommerce-infakt'                      => [
				'free' => false,
				'name' => 'WooCommerce InFakt',
				'team' => 'heroes',
			],
			'woocommerce-nokaut'                      => [
				'free' => false,
				'name' => 'WooCommerce Nokaut',
				'team' => 'heroes',
			],
			'woocommerce-paczka-w-ruchu'              => [
				'free' => false,
				'name' => 'WooCommerce Paczka w Ruchu',
				'team' => 'predators',
			],
			'woocommerce-paczkomaty-inpost'           => [
				'free' => false,
				'name' => 'WooCommerce inPost',
				'team' => 'predators',
			],
			'woocommerce-payment-status'              => [
				'free' => false,
				'name' => 'WooCommerce Payment Status',
				'team' => 'heroes',
			],
			'woocommerce-print-orders-address-labels' => [
				'free' => false,
				'name' => 'Print Orders Address Labels',
				'team' => 'heroes',
			],
			'woocommerce-wfirma'                      => [
				'free' => false,
				'name' => 'WooCommerce wFirma',
				'team' => 'heroes',
			],
			'wooifirma'                               => [
				'free' => false,
				'name' => 'WooCommerce iFirma',
				'team' => 'heroes',
			],
			'shopmagic-for-woocommerce'               => [
				'free' => true,
				'name' => 'ShopMagic for WooCommerce',
				'team' => 'heroes',
			],
		];
	}
}